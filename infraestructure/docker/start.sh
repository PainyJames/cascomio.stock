#!/bin/bash

docker-compose down

docker-compose up -d

curl 172.20.128.10:9000/clusters \
    --data "name=stocks&zkHosts=zookeeper1:2181,zookeeper2:2181,zookeeper3:2181,&kafkaVersion=0.9.0.1" \
    --retry 15 \
    --retry-connrefused \
    --retry-delay 5 \
    -X POST