package com.cascomio.stock.stream.producers

import akka.actor.{Actor, ActorLogging, Props}
import com.cascomio.stock.stream.clients.StockClient
import com.cascomio.stock.stream.clients.alphavantage.AlphaVantageClient
import com.cascomio.stock.stream.models.Company
import com.cascomio.stock.stream.producers.kafka.KafkaStockProducer

import scala.concurrent.ExecutionContext.Implicits.global

final class CompanyStocksActor(stockClient: Option[StockClient] = None, stockProducer: Option[StockProducer] = None)
  extends Actor with ActorLogging {
  private lazy val client = stockClient.getOrElse(new AlphaVantageClient)
  private lazy val producer = stockProducer.getOrElse(KafkaStockProducer)

  override def receive: Receive = {
    case company: Company => {
      val result = client.getStockData(company.symbol)
      result.map {
        case Some(stocks) => {
          println(s"Producing for ${stocks.metadata.symbol}")
          producer.produce(stocks)
        }
        case _ => None
      }
    }
  }
}

object CompanyStocksActor {
  def props(): Props = Props(new CompanyStocksActor())
}
