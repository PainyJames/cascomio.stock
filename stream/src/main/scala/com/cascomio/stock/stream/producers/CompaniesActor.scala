package com.cascomio.stock.stream.producers

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.cascomio.stock.stream.clients.CompanyClient
import com.cascomio.stock.stream.clients.nasdaq.NasdaqCompanyClient
import com.cascomio.stock.stream.models.Company

final class CompaniesActor(stockActors: Option[Map[String, ActorRef]] = None,
                           companyClient: Option[CompanyClient] = None)
  extends Actor with ActorLogging {

  import CompaniesActor._

  private lazy val client = companyClient.getOrElse(new NasdaqCompanyClient)
  private lazy val companies = client.getCompanies
  private lazy val actors = stockActors.getOrElse(createActorPerCompany(companies))

  override def receive: Receive = {
    case Start => {
      companies.take(40).foreach(self ! _)
    }
    case company: Company => {
      val actor = actors(company.symbol)
      actor ! company
    }
  }

  private def createActorPerCompany(companies: List[Company]): Map[String, ActorRef] = {
    (for {
      company <- companies
      actor = context.actorOf(CompanyStocksActor.props(), s"company-stock-${company.symbol}")
    } yield (company.symbol -> actor)).toMap
  }
}

object CompaniesActor {
  case class Start()
  def props(): Props = Props(new CompaniesActor())
}
