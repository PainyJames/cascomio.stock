package com.cascomio.stock.stream.clients.nasdaq

import com.cascomio.stock.stream.clients.CompanyClient
import com.cascomio.stock.stream.models.Company

import scala.io.Source

class NasdaqCompanyClient extends CompanyClient {
  val CompaniesUrl = "http://www.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=nasdaq&render=download"

  override def getCompanies(): List[Company] = {
    val content = Source.fromURL(CompaniesUrl).mkString
    parseCompanies(content)
  }

  private def parseCompanies(content: String): List[Company] = {
    val lines = content.split("\n").tail
    lines.map(l => {
      val values = l.replace("\"", "").split(",")
      new Company(values(1).trim, values(0).trim, values(5).trim, values(6).trim)
    })(collection.breakOut)
  }
}
