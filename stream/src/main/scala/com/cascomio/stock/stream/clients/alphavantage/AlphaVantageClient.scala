package com.cascomio.stock.stream.clients.alphavantage

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import com.cascomio.stock.stream.clients.StockClient
import com.cascomio.stock.stream.models.Stock
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.Future

class AlphaVantageClient extends StockClient
  with AlphaVantageStockSerializer {

  private lazy val config: Config = ConfigFactory.load()
  private lazy val client = Http()

  def getStockData(symbol: String): Future[Option[Stock]] = {
    val uri = getStockDataUri(symbol)
    val response = client.singleRequest(HttpRequest(uri = uri))
    parseStock(response)
  }

  private def getStockDataUri(symbol: String): String = {
    val alphaVantageKey = config.getString("alphaVantageKey")
    val uri = config.getString("alphaVantageUrl") +
      config.getString("alphaVantageStockUrl")
    uri.format(symbol, alphaVantageKey)
  }
}

object AlphaVantageClient extends StockClient {
  private lazy val alphaVantageClient = new AlphaVantageClient

  def getStockData(symbol: String): Future[Option[Stock]] = {
    alphaVantageClient.getStockData(symbol)
  }
}
