package com.cascomio.stock.stream.clients.alphavantage

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{ContentTypes, HttpResponse}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.cascomio.stock.stream.models.{Stock, StockData, StockMetadata}
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait AlphaVantageStockSerializer {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  def parseStock(response: Future[HttpResponse]): Future[Option[Stock]] = {
    response.flatMap(r => {
      val entity = r.entity.withContentType(ContentTypes.`application/json`)
      val json = Unmarshal(entity).to[String]
      parseJson(json)
    })
  }

  private def parseJson(json: Future[String]): Future[Option[Stock]] = {
    json.map(j => {
        val list = j.parseJson.convertTo[Map[String, JsObject]].toList
        if (list.isEmpty) None else Some(parseStock(list))
    })
  }

  private def parseStockMetadata(metadata: (String, JsObject)): StockMetadata = {
    metadata match {
      case (_, values) => {
        values.getFields("1. Information", "2. Symbol") match {
          case Seq(JsString(info), JsString(symbol)) =>
            new StockMetadata(info, symbol)
          case _ => throw new DeserializationException("Information and Symbol expected")
        }
      }
    }
  }

  private def parseStockData(data: (String, JsObject)): List[StockData] = {
    data match {
      case (_, values) => {
        val valuesList = values.convertTo[Map[String, Map[String, String]]]
        valuesList.map { case (date, stockValues) => {
            new StockData(date,
              stockValues.getOrElse("1. open", ""),
              stockValues.getOrElse("2. high", ""),
              stockValues.getOrElse("3. low", ""),
              stockValues.getOrElse("4. close", ""),
              stockValues.getOrElse("5. volume", "")
            )
          }
        }
      }.toList
    }
  }

  private def parseStock(list: List[(String, JsObject)]): Stock = {
    list match {
      case metadata :: data :: Nil => new Stock(parseStockMetadata(metadata), parseStockData(data))
      case _ => throw new DeserializationException("Wrong stock message")
    }
  }
}