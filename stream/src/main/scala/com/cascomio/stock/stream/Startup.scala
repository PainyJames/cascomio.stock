package com.cascomio.stock.stream

import akka.actor.ActorSystem
import com.cascomio.stock.stream.producers.CompaniesActor
import com.cascomio.stock.stream.producers.CompaniesActor.Start

import scala.concurrent.duration._

object Startup extends App {
  val system = ActorSystem()
  val props = CompaniesActor.props()
  val scheduler = system.scheduler
  implicit val executor = system.dispatcher
  val actor = system.actorOf(props, "companies")

  val cancellable = scheduler.schedule(
    initialDelay = 5 seconds,
    interval = 10 seconds,
    receiver = actor,
    message = Start)
}
