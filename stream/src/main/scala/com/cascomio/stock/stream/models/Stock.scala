package com.cascomio.stock.stream.models

import spray.json.DefaultJsonProtocol

case class StockMetadata(val info: String, val symbol: String)

case class StockData(val date: String,
                     val open: String,
                     val high: String,
                     val low: String,
                     val close: String,
                     val volume: String)

case class Stock(val metadata: StockMetadata, val data: List[StockData])

object StockJsonProtocol extends DefaultJsonProtocol {
  implicit val stockMetaDataFormat = jsonFormat2(StockMetadata)
  implicit val stockDataFormat = jsonFormat6(StockData)
  implicit val stockFormat = jsonFormat2(Stock)
}


