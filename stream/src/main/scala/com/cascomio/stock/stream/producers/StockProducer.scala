package com.cascomio.stock.stream.producers

import com.cascomio.stock.stream.models.Stock

trait StockProducer {
  def produce(stocks: Stock)
}
