package com.cascomio.stock.stream.models

case class Company(name: String, symbol: String, sector: String, industry: String)
