package com.cascomio.stock.stream.clients

import com.cascomio.stock.stream.models.Stock

import scala.concurrent.Future

trait StockClient {
  def getStockData(symbol: String) : Future[Option[Stock]]
}
