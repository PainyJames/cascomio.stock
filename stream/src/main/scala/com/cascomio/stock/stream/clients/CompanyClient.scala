package com.cascomio.stock.stream.clients

import com.cascomio.stock.stream.models.Company

import scala.concurrent.Future

trait CompanyClient {
  def getCompanies(): List[Company]
}
