package com.cascomio.stock.stream.producers.kafka

import java.util.Properties

import com.cascomio.stock.stream.models.Stock
import com.cascomio.stock.stream.models.StockJsonProtocol._
import com.cascomio.stock.stream.producers.StockProducer
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import spray.json._

object KafkaStockProducer extends StockProducer {
  private val config: Config = ConfigFactory.load()
  private val kafkaConfig = config.getConfig("kafka")
  private val topic: String = kafkaConfig.getString("topic")
  private val producer: KafkaProducer[String, String] = getProducer

  override def produce(stocks: Stock) = {
    val t = System.currentTimeMillis()
    val json = stocks.toJson.prettyPrint
    val data = new ProducerRecord[String, String](topic, t.toString, json)
    producer.send(data, (_,_) => {
      producer.close()
    })
  }

  private def getProducer = {
    val props = new Properties()
    kafkaConfig.entrySet().forEach(p => props.put(p.getKey, p.getValue.unwrapped))
    new KafkaProducer[String, String](props)
  }
}
