package com.cascomio.stock.stream.producers

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.cascomio.stock.stream.clients.StockClient
import com.cascomio.stock.stream.models.{Company, Stock, StockMetadata}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class CompanyStocksActorTests extends TestKit(ActorSystem(classOf[CompanyStocksActorTests].getSimpleName))
  with ImplicitSender
  with WordSpecLike
  with MockFactory
  with BeforeAndAfterAll {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Company stocks actor" should {
    "send a message to the broker when stocks are received" in {
      within(300 millis) {
        val text = "Foo";
        val message = new Company(text, text, text, text)
        val stockMetadata = new StockMetadata(text, text)
        val stock = new Stock(stockMetadata, List())
        val stocksResult = Future(Some(stock))
        val stockClient = mock[StockClient]
        val stockProducer = mock[StockProducer]
        (stockClient.getStockData(_: String)).expects(text).returning(stocksResult).once
        (stockProducer.produce(_: Stock)).expects(stock).once
        val props = Props(new CompanyStocksActor(Some(stockClient), Some(stockProducer)))
        val companyStocksActor = system.actorOf(props)

        companyStocksActor ! message

        expectNoMsg
      }
    }
  }
}
