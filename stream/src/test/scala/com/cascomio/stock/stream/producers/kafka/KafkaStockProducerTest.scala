package com.cascomio.stock.stream.producers.kafka

import com.cascomio.stock.stream.models.{Stock, StockData, StockMetadata}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class KafkaStockProducerTest extends WordSpecLike
  with BeforeAndAfterAll
  with Matchers {
  "Kafka stock producer" should {
    "produce stock messages with no errors" in {
      val kafkaProducer = KafkaStockProducer
      val stockMetadata = new StockMetadata("foo","bar")
      val stockData = new StockData("","","","","","")
      val stocks = new Stock(stockMetadata, List(stockData))

      kafkaProducer.produce(stocks)
    }
  }
}
