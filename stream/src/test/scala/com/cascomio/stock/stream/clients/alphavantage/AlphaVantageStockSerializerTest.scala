package com.cascomio.stock.stream.clients.alphavantage

import akka.http.scaladsl.model._
import com.cascomio.stock.stream.models.Stock
import org.scalatest._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class AlphaVantageStockSerializerTest extends WordSpecLike
  with BeforeAndAfterAll
  with Matchers {
  "A valid stock message" should {
    "be parsed correctly" in {
      val payload =
        """{
        "Meta Data": {
          "1. Information": "Daily Prices (open, high, low, close) and Volumes",
          "2. Symbol": "MSFT",
          "3. Last Refreshed": "2017-09-19",
          "4. Output Size": "Compact",
          "5. Time Zone": "US/Eastern"
        },
        "Time Series (Daily)": {
          "2017-09-19": {
            "1. open": "75.2100",
            "2. high": "75.7100",
            "3. low": "75.0100",
            "4. close": "75.4400",
            "5. volume": "15765725"
          },
          "2017-09-18": {
            "1. open": "75.2300",
            "2. high": "75.9700",
            "3. low": "75.0400",
            "4. close": "75.1600",
            "5. volume": "22730355"
          }
        }
      }"""
      val response = Future(HttpResponse(status = StatusCode.int2StatusCode(200),
        entity = HttpEntity.apply(ContentTypes.`application/json`, payload)))
      val alphaVantageClient = new AlphaVantageClient

      val result = alphaVantageClient.parseStock(response)
      val stock = Await.result(result, 3000 millis).get

      stock.metadata.info shouldBe "Daily Prices (open, high, low, close) and Volumes"
      stock.data.length shouldBe 2
      stock.data(0).open shouldBe "75.2100"
      stock.data(1).volume shouldBe "22730355"
    }
  }
}
