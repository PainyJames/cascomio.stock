package com.cascomio.stock.stream.clients.nasdaq

import org.scalatest._

class NasdaqCompanyClientTest extends WordSpecLike {
  "Nasdaq client" should {
    val nasdaqCompanyClient = new NasdaqCompanyClient
    "retrieve listed companies" in {
      val companies = nasdaqCompanyClient.getCompanies

      assert(companies.length > 0)
      assert(companies.exists(_.symbol == "MSFT"))
    }
  }
}
