package com.cascomio.stock.stream.producers

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import com.cascomio.stock.stream.models.Company
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

import scala.concurrent.duration._

class CompaniesActorTests extends TestKit(ActorSystem(classOf[CompaniesActorTests].getSimpleName))
  with ImplicitSender
  with WordSpecLike
  with MockFactory
  with BeforeAndAfterAll {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Company actor" should {
    "send a message to the actor taking care of a certain stock" in {
      val companyStockActor = TestActorRef(new CompanyStocksActor)
      val probe = TestProbe()
      probe watch companyStockActor
      val symbol = "Foo"
      val message = new Company("", symbol, "", "")
      val stockActors = Map[String, ActorRef]((symbol, probe.ref))
      val props = Props(new CompaniesActor(Some(stockActors)))
      val companiesActor = system.actorOf(props)

      companiesActor ! message

      probe.expectMsg(300 millis, message)
    }
  }
}
