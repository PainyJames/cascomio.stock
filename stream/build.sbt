name := "cascomio.stock.stream"

version := "0.1.0"

scalaVersion := "2.12.1"

val AkkaHttpVersion = "10.0.10"
val AkkaVersion = "2.4.17"
val SprayVersion = "1.3.3"
val CirceVersion = "0.8.0"
val ScalaTestVersion = "3.0.1"
val ScalaMockVersion = "3.6.0"
val KafkaVersion = "0.11.0.0"

resolvers ++= Seq("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Java.net Maven2 Repository" at "http://download.java.net/maven/2/",
  Resolver.bintrayRepo("hseeberger", "maven"))

libraryDependencies ++= {
  Seq(
    "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
    "com.typesafe.akka" %% "akka-slf4j"      % AkkaVersion,
    "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
    "io.spray" %% "spray-json" % SprayVersion,
    "org.apache.kafka" % "kafka-clients" % KafkaVersion,
    "org.scalatest" %% "scalatest" % ScalaTestVersion % "test",
    "com.typesafe.akka" %% "akka-testkit" % AkkaVersion % "test",
    "org.scalamock" %% "scalamock-scalatest-support" % ScalaMockVersion % "test"
  )
}

mainClass in (Compile, run) := Some("com.cascomio.stock.stream.Startup")

mainClass in (Compile, packageBin) := Some("com.cascomio.stock.stream.Startup")