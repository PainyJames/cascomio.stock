#!/bin/bash

function ingest {
    cd ingest;
    sbt ";clean;assembly"
    rm cascomio-stock-ingest.jar
    cp $(find . -name *jar | head -n 1) cascomio-stock-ingest.jar
    docker cp cascomio-stock-ingest.jar spark:/tmp
    docker exec spark \
        spark-submit \
            --class com.cascomio.stock.ingest.Startup \
            --master yarn-cluster \
            --conf spark.executor.instances=1 \
            --conf spark.executor.memory=512m \
            --conf spark.driver.memory=512m \
            /tmp/cascomio-stock-ingest.jar
}

while [ $# -gt 0 ]
do
    case "$1" in
        stream)
            (cd stream; sbt "run")
        ;;
        ingest)
            ingest
        ;;
        *)
            exit 0
        ;;
    esac
    shift
done