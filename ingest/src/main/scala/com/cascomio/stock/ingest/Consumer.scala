package com.cascomio.stock.ingest

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.collection.mutable

object Consumer {
  private val config: Config = ConfigFactory.load()
  private val sparkConfig = config.getConfig("spark")
  private val kafkaConfig = config.getConfig("kafka")
  private val kafkaParams = getKafkaParams
  private val topics = kafkaConfig.getString("topic")
  private val conf = new SparkConf()
    .setAppName(sparkConfig.getString("appName"))
    .setMaster("local[2]")

  def consume() = {
    val streamingContext = new StreamingContext(conf, Seconds(1))
    val stream = KafkaUtils.createDirectStream[String, String](
      streamingContext,
      PreferConsistent,
      Subscribe[String, String](topics.split(","), kafkaParams)
    )

    val stocks = stream.map(record => {
      (record.value)
    })

    stocks.foreachRDD(rdd => {
      if(!rdd.isEmpty) {
        rdd.saveAsTextFile(s"/tmp/stocks/${java.time.OffsetDateTime.now.toEpochSecond}")
      }
    })

    streamingContext.start
    streamingContext.awaitTermination
  }

  private def getKafkaParams = {
    val iterator = kafkaConfig.entrySet().iterator()
    val map = mutable.HashMap[String,String]()
    while(iterator.hasNext()){
      val next = iterator.next()
      map.put(next.getKey, next.getValue().unwrapped().toString)
    }
    map
  }
}
