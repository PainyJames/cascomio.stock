name := "cascomio.stock.ingest"

version := "0.1.0"

scalaVersion := "2.11.8"

val SparkVersion = "2.2.0"
val ScalaTestVersion = "3.0.1"
val ScalaMockVersion = "3.6.0"
val ConfigVersion = "1.3.1"

resolvers ++= Seq("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Java.net Maven2 Repository" at "http://download.java.net/maven/2/",
  "Hortonworks Repository" at "http://repo.hortonworks.com/content/repositories/releases/",
  "Hortonworks Jetty Maven Repository" at "http://repo.hortonworks.com/content/repositories/jetty-hadoop/",
  Resolver.bintrayRepo("hseeberger", "maven"))

libraryDependencies ++= {
  Seq(
    "org.apache.spark" % "spark-streaming_2.11" % SparkVersion,
    "org.apache.spark" % "spark-streaming-kafka-0-10_2.11" % SparkVersion,
    "com.typesafe" % "config" % ConfigVersion,
    "org.scalatest" %% "scalatest" % ScalaTestVersion % Test,
    "org.scalamock" %% "scalamock-scalatest-support" % ScalaMockVersion % Test
  )
}

assemblyMergeStrategy in assembly := {
  case PathList("org","aopalliance", xs @ _*) => MergeStrategy.last
  case PathList("javax", "inject", xs @ _*) => MergeStrategy.last
  case PathList("javax", "servlet", xs @ _*) => MergeStrategy.last
  case PathList("javax", "activation", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", xs @ _*) => MergeStrategy.last
  case PathList("com", "google", xs @ _*) => MergeStrategy.last
  case PathList("com", "esotericsoftware", xs @ _*) => MergeStrategy.last
  case PathList("com", "codahale", xs @ _*) => MergeStrategy.last
  case PathList("com", "yammer", xs @ _*) => MergeStrategy.last
  case "about.html" => MergeStrategy.rename
  case "META-INF/ECLIPSEF.RSA" => MergeStrategy.last
  case "META-INF/mailcap" => MergeStrategy.last
  case "META-INF/mimetypes.default" => MergeStrategy.last
  case "plugin.properties" => MergeStrategy.last
  case "log4j.properties" => MergeStrategy.last
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

mainClass in (Compile, run) := Some("com.cascomio.stock.ingest.Startup")

mainClass in (Compile, packageBin) := Some("com.cascomio.stock.ingest.Startup")